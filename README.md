# frontend-mysql-cat-cafe-lab

In this lab, we will learn to pull information from our MySQL database to our front-end React app

Figma design here: https://www.figma.com/file/enllL85WtXAG2sz8NduBMd/cat-cafe-connecting-msql?node-id=0%3A1

# Instructions

## Part 1 - GET Request for all the cats

Goal: Fetch the GET request to get all cats. 

Success: You have completed this step when you can see all the cats on http://localhost:3000/

1. Create a new React app 

2. In React, create a `AllCats.jsx` component that will be used/imported in `App.js`    

```
 <div>
      <h1>Jen's Cat Adoption Cafe Website</h1>
        <div>
            <p>Cat Name</p>
            <img src="" alt="" width="300" height="300" />
        </div>
</div>
```

3. IN THE BACKEND PROJECT FOLDER - `npm install cors` and set up cors in `index.js`
- what is cors: allows our front-end on localhost:3000 to access the back-end on localhost:3001
- adding cors: https://www.npmjs.com/package/cors

4. IN THE FRONTEND PROJECT FOLDER - and add a `proxy` to the end of `package.json` so you can fetch api calls from the localhost:3001 api routes we made in the backend
- adding proxy: https://dev.to/loujaybee/using-create-react-app-with-express

5. In `AllCats.jsx`, create a fetch call so you can see all the cats on http://localhost:3000/
- You will need to use React Hooks (`useState and useEffect`) to change the state
- Hint: Check out UseEffect at ComponentDidMount section at the very bottom of this article: https://medium.com/better-programming/how-to-fetch-data-from-an-api-with-react-hooks-9e7202b8afcd
- `Map` the array that you 'fetched' so all cats appear on the page



## "The client calls you for a meeting"

Jen forgot to tell you that her cat cafe is now accepting new cats to house. She has decided that she also wants to have a page where anyone can register a adoptable cat to her website and drop it off at the shop later.

## Part 2 - React routes and viewing a cat profile

Goal: We want our users to access the "cat profile" page when they type the url in the browser.
Success: When you go to http://localhost:3000/cats/1, you should see the cat profile of the first cat in your database.
Additionally, each cat on the list (in http://localhost:3000/) has "view profile" button that takes you to their profile page.

In order to do this, we'll need to install React router.

Documentation is here:
https://reactrouter.com/web/api/BrowserRouter

1. `npm install --save react-router-dom`

### NOTE: the below instructions have changed for browserRouter - please see this repo for how to go about it
Updated Repo is here: https://gitlab.com/fs1030-open-labs/answers/updated-frontend-mysql-cat-cafe-lab -> file changes in app.js, index.js and CatProfile.js

2. `import { BrowserRouter } from "react-router-dom"` to `index.js` and add `<BrowserRouter>` between your `<App>` tags

4. `import { Route } from "react-router-dom"` in `App.js` and direct your AllCats component to '/cats' route `<Route exact path="/" component={AllCats} />`

5. Create another component `CatProfile.jsx` and route it `<Route exact path="/cats/:id" component={CatProfile} />`

6. In `AllCats.jsx` add a `button` that says "View Cat Profile" - when a user clicks this button, it should take them to the corresponding cat profile.
- hint: you will need to `import { useHistory } from "react-router-dom"`
- Documentation here: https://reactrouter.com/web/api/Hooks

6. [on your own] In `CatProfile.jsx` complete the fetch (hint it is the same as Part #1 step #5) to get the cat by ID


## Part 3 - POST request to add a cat to the list

Goal: We want our users to be able to add a cat to our current list.

Success: When you go to http://localhost:3000/add-cat, you should see a form to add a cat to the list. If a user fills out the form and presses "Submit", they are redirected to http://localhost:3000/ to see the new list of cats.

1. Create another component `AddCatForm.jsx` and route it `<Route exact path="/add-cat" component={AddCatForm} />`

Component info below:

```<div>
      <h1>This is the add cat form</h1>
      <form>
        <div>
          <label>
            Name:
            <input
              type="text"
              name="name"
            />
          </label>
        </div>
        <div>
          <label>
            Photo:
            <input
              type="text"
              name="image"
            />
          </label>
        </div>
        <input type="submit" value="Submit" />
      </form>
    </div>
```

2. In `AddCatForm.jsx` - we now want to be able to track what the user is typing into the form and be able to submit to the database. 

- First, we want to set the initial state of the form with `useState`, note we set it to be blank since a user hasn't added a cat yet

`const [newCat, setNewCat] = useState({ name: "", image: "", });`

- Second, we now want to track what the user is typing in the form, so we create a `handleChange` function to track the state

` const handleChange = (event) => { setNewCat((prevState) => ({ ...prevState, [event.target.name]: event.target.value, })); };`

- connect the form values and check dev tools to see if state is changing (hint: `onChange`)
- https://reactjs.org/docs/forms.html

- Third, we now want to submit the cat that the user typed in the form, so we create a `handleSubmit` function to make a fetch post api call to submit the cat to the database.


## Part 4 - DELETE request to remove a cat profile
Goal: We want our users to be able to delete a cat profile.

Success: When a user goes to a cat profile, there is a "delete" button to be able to delete the cat. Once they click "delete", the user is redirected to the all cats page.

1. In `CatProfile.jsx` - add a `button` so that when it's `onClick`, the `handleDelete` function runs. You can test to see if the button is connected by adding `console.log("test")` in your `handleDelete` function. If you click the button and see "test" in the console, then you know the button is connected.

2. In `handleDelete` - add your fetch delete call and redirect to the all cats page.


## Part 5 - PUT request to edit a cat profile
Goal: We want our users to be able to edit a cat profile.

Success: When a user goes to a cat profile, there is an "edit" button to be able to edit the cat. Once they click "edit", a form opens where they can edit and update the cat profile. Once they click "submit", they are redirected to the all cats page.

1. In `CatProfile.jsx` - add an "edit cat" `button` so that when it's `onClick`, the `handleEdit` function runs. 

2. When the "edit" button is clicked, show the form
- hint: you will need to make another state for this. The initial state has the form hidden, then the state changes when the button is clicked.
`const [form, setForm] = useState({ display: "none" });`

Component info below:

```<form style={form}>
        <div>
          <label>
            Name:
            <input
              type="text"
              name="name"
            />
          </label>
        </div>
        <div>
          <label>
            Photo:
            <input
              type="text"
              name="image"
            />
          </label>
        </div>
        <input type="submit" value="Submit" />
    </form>
```

3.  When the "edit" button is clicked, show the form WITH the pre-filled cat information.
- you will need to make another state for this. The initial state has the form blank, then the state changes to the pre-filled cat when the button is clicked.
- That means that the `handleEdit` function should set the two states to display the form AND prefill the cat
- Create a separate state `const [cat, setCat] = useState({ name: "", image: "" });`
- Tweak your form to so each input box has `value={cat.name}` and `value={cat.image}`

4. Now you want to track the changes that a user made to the cat and submit the form.
- hint: Similar to Post Request part #3 step #2. The only difference is that we already have our initial state which is our pre-filled cat.
